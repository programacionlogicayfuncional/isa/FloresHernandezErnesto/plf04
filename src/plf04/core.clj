(ns plf04.core)


(defn stringE-1
  [xs]
  (letfn [(f [x y]
            (if (empty? x)
              (if (and (> y 0) (< y 4))
                true false)
              (if (= \e (first x))
                (f (rest x) (inc y))
                (f (rest x) y))))]
    (f xs 0)))


(stringE-1 "hello")
(stringE-1 "Heelle")
(stringE-1 "Heelele")
(stringE-1 "Hll")
(stringE-1 "e")
(stringE-1 "")

(defn stringE-2
  [x]
  (letfn [(f [y acc]
            (if (empty? y)
              (if (and (> acc 0) (< acc 4))
                true false)
              (if (identical? \e (first y))
                (f (subs y 1) (inc acc))
                (f (subs y 1) acc))))]
    (f x 0)))

(stringE-2 "hello")
(stringE-2 "Heelle")
(stringE-2 "Heelele")
(stringE-2 "Hll")
(stringE-2 "e")
(stringE-2 "")

(defn stringTimes-1
  [xs ys]
  (letfn [(f [x y]
            (if (or (== 0 y))
              ""
              (str x (f x (dec y)))))]
    (f xs ys)))


(stringTimes-1 "Hi" 2)
(stringTimes-1 "Hi" 3)
(stringTimes-1 "Hi" 1)
(stringTimes-1 "Hi" 0)
(stringTimes-1 "Hi" 5)
(stringTimes-1 "Oh Boy!" 2)
(stringTimes-1 "x" 4)
(stringTimes-1 "" 4)
(stringTimes-1 "code" 2)
(stringTimes-1 "code" 3)

(defn stringTimes-2
  [xs ys]
  (letfn [(f [x y acc]
            (if (or (zero? y) (empty? x))
              acc
              (str x (f x (dec y) (str acc)))))]
    (f xs ys "")))

(stringTimes-2 "Hi" 2)
(stringTimes-2 "Hi" 3)
(stringTimes-2 "Hi" 1)
(stringTimes-2 "Hi" 0)
(stringTimes-2 "Hi" 5)
(stringTimes-2 "Oh Boy!" 2)
(stringTimes-2 "x" 4)
(stringTimes-2 "" 4)
(stringTimes-2 "code" 2)
(stringTimes-2 "code" 3)


(defn frontTimes-1
  [xs ys]
  (letfn [(f [x y]
            (if (zero? x)
              nil
              (str (subs y 0 3) (f (dec x) y))))]
    (f ys xs)))

(frontTimes-1 "Chocolate" 3)
(frontTimes-1 "Chocolate" 2)
(frontTimes-1 "Abcc" 3)
(frontTimes-1 "  Ab" 4)
(frontTimes-1 "   " 4)

(defn frontTimes-2
  [xs ys]
  (letfn [(f [x y acc]
            (if (zero? x)
              acc
              (f (dec x) y (str acc (subs y 0 3)))))]
    (f ys xs "")))

(frontTimes-2 "Chocolate" 3)
(frontTimes-2 "Chocolate" 2)
(frontTimes-2 "Abc" 3)
(frontTimes-2 "  Ab" 4)
(frontTimes-2 "   " 4)

(defn countXX-1
  [xs]
  (letfn [(f [x y]
            (if (empty? x)
              y
              (if (and (= \x (first x)) (= \x (first (rest x))))
                (f (rest x) (inc y))
                (f (rest x) y))))]
    (f xs 0)))

(countXX-1 "abcxx")
(countXX-1 "xxx")
(countXX-1 "xxxx")
(countXX-1 "abc")
(countXX-1 "Hexxo thxxe")

(defn countXX-2
  [xs]
  (letfn [(f [x  acc]
            (if (empty? x)
              acc
              (if (and (= \x (first x)) (= \x (first (rest x))))
                (f (rest x) (inc acc))
                (f (rest x) acc))))]
    (f xs 0)))

(countXX-2 "abcxx")
(countXX-2 "xxx")
(countXX-2 "xxxx")
(countXX-2 "abc")
(countXX-2 "Hexxo thxxe")

(defn stringSplosion-1
  [xs]
  (letfn [(f [x y]
            (if (or (empty? x)
                    (>= y (count x)))
              ""
              (str (subs x 0 (+ y 1)) (f x (inc y)))))]
    (f xs 0)))

(stringSplosion-1 "code")
(stringSplosion-1 "abc")
(stringSplosion-1 "ab")
(stringSplosion-1 "x")
(stringSplosion-1 "fade")
(stringSplosion-1 "There")
(stringSplosion-1 "Kitten")
(stringSplosion-1 "bay")
(stringSplosion-1 "Good")

(defn stringSplosion-2
  [xs]
  (letfn [(f [x y acc]
            (if (or (empty? x)
                    (>= y (count x)))
              acc
              (f x (inc y) (str acc (subs x 0 (+ y 1))))))]
    (f xs 0 "")))

(stringSplosion-2 "code")
(stringSplosion-2 "abc")
(stringSplosion-2 "ab")
(stringSplosion-2 "x")
(stringSplosion-2 "fade")
(stringSplosion-2 "There")
(stringSplosion-2 "Kitten")
(stringSplosion-2 "bay")
(stringSplosion-2 "Good")

(defn array123-1
  [x]
  (letfn [(f [xs]
            (if (empty? xs)
              false
              (if (>= (count xs) 3)

                (if (and (== (first xs) 1) (== (first (rest xs)) 2) (== (first (rest (rest xs))) 3))
                  true
                  (f (rest xs)))
                false)))]
    (f x)))

(array123-1 [1 1 2 3 1])
(array123-1 [1 1 2 4 1])
(array123-1 [1 1 2 1 2 3])
(array123-1 [1 1 2 1 2 1])
(array123-1 [1 2 3 1 2 3])
(array123-1 [1 2 3])
(array123-1 [1 1 1])
(array123-1 [1 2])
(array123-1 [1])
(array123-1 [])

(defn stringX-2
  [x]
  (letfn [(f [x y acc]
            (if (empty? x)
              acc
              (if (== 0 y)
                (str (first x) (f (rest x) (inc y) (str acc)))
                (if (and (= \x (first x)) (> (count x) 1))
                  (f (rest x) (inc y) (str acc))
                  (str (first x) (f (rest x) (inc y) acc))))))]
    (f x 0 "")))

(stringX-2 "xxHxix")
(stringX-2 "abxxxcd")
(stringX-2 "xabxxxcdx")
(stringX-2 "xKittenx")
(stringX-2 "Hello")
(stringX-2 "xx")
(stringX-2 "x")
(stringX-2 "")

(defn altPairs-1
  [xs]
  (letfn [(f [x y]
            (if (== (count x) y)
              ""
              (if (or (== y 0) (== y 1) (== y 4) (== y 5) (== y 8) (== y 9))
                (str (subs x y (+ y 1)) (f x (inc y)))
                (f x (inc y)))))]
    (f xs 0)))

(altPairs-1 "kitten")
(altPairs-1 "Chocolate")
(altPairs-1 "CodingHorror")
(altPairs-1 "yak")
(altPairs-1 "ya")
(altPairs-1 "")
(altPairs-1 "ThisThatTheOther")

(defn altPairs-2
  [xs]
  (letfn [(f [x y acc]
            (if (== (count x) y)
              acc
              (if (or (== y 0) (== y 1) (== y 4) (== y 5) (== y 8) (== y 9))
                (f x (inc y) (str acc (subs x y (+ y 1))))
                (f x (inc y) acc))))]
    (f xs 0 "")))

(altPairs-2 "kitten")
(altPairs-2 "Chocolate")
(altPairs-2 "CodingHorror")
(altPairs-2 "yak")
(altPairs-2 "ya")
(altPairs-2 "")
(altPairs-2 "ThisThatTheOther")

(defn stringYak-1
  [xs]
  (letfn [(f [x y]
            (if (empty? x)
              ""
              (if (and (= (first x) \y) (= (first (rest x)) \a) (= (first (rest (rest x))) \k) (> (count x) 1))
                (f (rest (rest (rest x))) (inc y))
                (str (first x) (f (rest x) (inc y))))))]
    (f xs 0)))

(stringYak-1 "tareaplf")
(stringYak-1 "yakpak")
(stringYak-1 "pakyak")
(stringYak-1 "yak123ya")
(stringYak-1 "yak")
(stringYak-1 "yakxxxyak")
(stringYak-1 "HiyakHi")
(stringYak-1 "xxxyakyyyakzzz")

(defn has271
  [xs]
  (letfn [(f [ys]
            (if (or (<= (count ys) 2) (empty? ys))
              false
              (if (and
                   (== (first (rest ys)) (+ (first ys) 5))
                   (<= (if (pos? (- (first (rest (rest ys))) (dec (first ys))))
                         (- (first (rest (rest ys))) (dec (first ys)))
                         (* -1 (- (first (rest (rest ys))) (dec (first ys))))) 2))
                true
                (f (rest ys)))))]
    (f xs)))

(has271 [1,2,7,1])
(has271 [1, 2, 8, 1])
(has271 [2, 7, 1])
(has271 [3, 8, 2])
(has271 [2, 7, 3])
(has271 [2, 7, 4])
(has271 [2, 7, -1])
(has271 [2, 7, -2])
(has271 [4, 5, 3, 8, 0])
(has271 [2, 7, 5, 10, 4])
(has271 [2, 7, -2, 4, 9, 3])
(has271 [2, 7, 5, 10, 1])
(has271 [2, 7, -2, 4, 10, 2])
(has271 [1, 1, 4, 9, 0])
(has271 [1, 1, 4, 9, 4, 9, 2])